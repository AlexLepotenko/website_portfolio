namespace Web_Site_Portfolio.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class WebPortfolioDb : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.MyNews",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        NameProject = c.String(nullable: false),
                        TimeNew = c.DateTime(nullable: false),
                        descriptionNew = c.String(nullable: false),
                        MainImageName = c.String(nullable: false),
                        ImageIconProject = c.Binary(),
                        TextNew = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.MyProjects",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        NameProject = c.String(nullable: false),
                        TimeProject = c.DateTime(nullable: false),
                        descriptionProject = c.String(nullable: false),
                        MainImageName = c.String(nullable: false),
                        ImageIconProject = c.Binary(),
                        TextProject = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.MyProjects");
            DropTable("dbo.MyNews");
        }
    }
}
