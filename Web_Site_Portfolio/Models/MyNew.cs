﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;



namespace Web_Site_Portfolio.Models
{
    public class MyNew
    {
        [Key]
        public int Id { get; set; }

        [Display(Name = "Название новости")]
        [Required(ErrorMessage = "Введите название новости")]
        public string NameProject { get; set; }

        [Display(Name = "Время загрузки новости:")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        [Required(ErrorMessage = "Введите Время загрузки изображения!!!")]
        public DateTime TimeNew { get; set; }

        [Display(Name = "Краткое описание")]
        [Required(ErrorMessage = "Введите краткое описание")]
        public string descriptionNew { get; set; }

        [Display(Name = "Имя изображения")]
        [Required(ErrorMessage = "Введите название изображения")]
        public string MainImageName { get; set; }

        public byte[] ImageIconProject { get; set; }

        [Display(Name = "Основная разметка статьи")]
        [Required(ErrorMessage = "Введите основную разметку статьи")]
        public string TextNew{ get; set; }//поле где будет храниться основная разметка поста
    }
}