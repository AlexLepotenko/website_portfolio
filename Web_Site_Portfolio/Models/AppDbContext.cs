﻿using System.Data.Entity;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Web_Site_Portfolio.Authentications;


namespace Web_Site_Portfolio.Models
{
    public class AppDbContext: IdentityDbContext<AppUser>
    {
        public AppDbContext() : base("name=WebPortfolioDb") { }

        public DbSet<MyNew> ListNews { get; set; }//контекст данных новостей
        public DbSet<MyProject> ListProject { get; set; }//контекст данных изображений для товара

        static AppDbContext()
        {
            Database.SetInitializer<AppDbContext>(new IdentityDbInit());
        }

        public static AppDbContext Create()
        {
            return new AppDbContext();
        }
    }

    public class IdentityDbInit : DropCreateDatabaseIfModelChanges<AppDbContext>
    {
        protected override void Seed(AppDbContext context)
        {
            PerformInitialSetup(context);
            base.Seed(context);
        }
        public void PerformInitialSetup(AppDbContext context)
        {
            //AppUserManager userMgr = new AppUserManager(new UserStore<AppUser>(context));
            //AppRoleManager roleMgr = new AppRoleManager(new RoleStore<AppRole>(context));

            //string roleName = "ADMIN";
            //string userName = "AlexADMIN";
            //string password = "481516234250Ak32";
            //string email = "dante.l.levi93@gmail.com";

            //if (!roleMgr.RoleExists(roleName))
            //{
            //    roleMgr.Create(new AppRole(roleName));
            //}

            //AppUser user = userMgr.FindByName(userName);
            //if (user == null)
            //{
            //    userMgr.Create(new AppUser { UserName = userName, Email = email },
            //        password);
            //    user = userMgr.FindByName(userName);
            //}

            //if (!userMgr.IsInRole(user.Id, roleName))
            //{
            //    userMgr.AddToRole(user.Id, roleName);
            //}
        }



    }


}