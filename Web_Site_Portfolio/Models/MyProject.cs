﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;



namespace Web_Site_Portfolio.Models
{
    public class MyProject
    {
        [Key]
        public int Id { get; set; }

        [Display(Name = "Название проекта")]
        [Required(ErrorMessage = "Введите название проекта")]
        public string NameProject { get; set; }

        [Display(Name = "Время загрузки изображения:")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        [Required(ErrorMessage = "Введите Время загрузки изображения!!!")]
        public DateTime TimeProject { get; set; }

        [Display(Name = "Краткое описание")]
        [Required(ErrorMessage = "Введите краткое описание")]
        public string descriptionProject { get; set; }
       
        [Display(Name = "Имя изображения")]
        [Required(ErrorMessage = "Введите название изображения")]
        public string MainImageName { get; set; }

        public byte[] ImageIconProject { get; set; }

        [Display(Name = "Основная разметка проекта")]
        [Required(ErrorMessage = "Введите основную разметку проекта")]
        public string TextProject { get; set; }//поле где будет храниться основная разметка проекта
    }
}