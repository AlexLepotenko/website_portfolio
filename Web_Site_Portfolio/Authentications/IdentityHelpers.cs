﻿using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity.Owin;

namespace Web_Site_Portfolio.Authentications
{
    public static class IdentityHelpers
    {
        public static MvcHtmlString GetUserName(this HtmlHelper html, string id)
        {
            AppUserManager mgr = HttpContext.Current
                .GetOwinContext().GetUserManager<AppUserManager>();

            return new MvcHtmlString(mgr.FindByIdAsync(id).Result.UserName);
        }
    }
}